<?php  
//CONFIGURAR EL DIRECTORIO DE LOS DIARIOS
$directorio='.'; //'.' para la misma carpeta donde esta el script

//Valores constantes
$meses=['','enero', 'febrero', 'marzo', 'abril', 'mayo', 'junio', 'julio', 'agosto', 'septiembre', 'octubre', 'noviembre', 'diciembre'];
$dir=opendir($directorio);
$anyo=0;
$mes=0;

//La idea es crear primero un vector con todos los dias, y generar luego el resto
$vectorelementos=[];
while($elemento=readdir($dir)){
	if(is_dir($directorio.'/'.$elemento)){
		if(substr($elemento,0,2)=='20'){
			if(is_file($directorio.'/'.$elemento.'/diario.txt')){
				$vectorelementos[]=$elemento;
			}
		}
	}
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Script para leer diarios - BOOTSTRAP3</title>

	<link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-theme.min.css" rel="stylesheet">

</head>
<body>
	<section class="container">
		<a name="arriba"></a>
		<header>
			<h1>Script para leer diarios - <small>CON BOOTSTRAP3</small></h1>
			<hr>
		</header>
		<nav>
			<header>
				<h1><small>Indice de contenidos</small></h1>
			</header>
			<?php  
			for($i=0; $i<count($vectorelementos); $i++){
				?>
				<a href="#<?php echo $vectorelementos[$i]; ?>">
					<?php 
					$anyodirectorio=substr($vectorelementos[$i],0,4);
					$mesdirectorio=intval(substr($vectorelementos[$i],4,2));
					$diadirectorio=substr($vectorelementos[$i],6,2);
					echo $diadirectorio.' de '.$meses[$mesdirectorio].' de '.$anyodirectorio; 
					?>
				</a><br>
				<?php
			}
			?>
			<hr>
		</nav>
		<section>
		<?php  
		//Script para mostrar todos los diarios de forma adecuada
		for($i=0; $i<count($vectorelementos); $i++){
			$anyodirectorio=substr($vectorelementos[$i],0,4);
			$mesdirectorio=intval(substr($vectorelementos[$i],4,2));
			$diadirectorio=substr($vectorelementos[$i],6,2);
			if($anyo!=$anyodirectorio){
				$anyo=$anyodirectorio;
				echo '<h2>Año '.$anyo.'</h2><hr>';
			}
			if($mes!=$mesdirectorio){
				$mes=$mesdirectorio;
				echo '<h3>'.strtoupper($meses[$mes]).' de '.$anyo.'</h3>';
			}
			echo '<h4><a name="'.$vectorelementos[$i].'"></a>'.$diadirectorio.' de '.$meses[$mesdirectorio].' de '.$anyodirectorio.' - <small><a href="'.$directorio.'/'.$vectorelementos[$i].'/diario.txt">Ver diario en Texto</a></small> - <small><a href="'.$directorio.'/'.$vectorelementos[$i].'/">Ver directorio</a></small> - <small><a href="#arriba">Subir</a></small></h4>';
			echo '<pre>';
			$contenido=file_get_contents($directorio.'/'.$vectorelementos[$i].'/diario.txt');
			//include($directorio.'/'.$vectorelementos[$i].'/diario.txt');
			echo htmlentities($contenido);
			echo '</pre>';
			echo '<hr>';
		}
		?>
		</section>
	</section>

	<script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>

</body>
</html>